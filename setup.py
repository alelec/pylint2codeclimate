from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='pylint2codeclimate',
    use_scm_version=True,  # This will generate the version number from git tags
    description='pylint reporter class to export results in codeclimate format',
    long_description=long_description,
    url='https://gitlab.com/alelec/pylint2codeclimate',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['pylint2codeclimate', 'message_overrides'],
    setup_requires=['setuptools_scm'],
)
